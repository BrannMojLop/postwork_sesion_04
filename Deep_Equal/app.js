function deepEqual(a, b) {
    if (typeof a === "object" && typeof b === "object") {
        let objA = Object.keys(a);
        let objB = Object.keys(b);
        if (objA.length === objB.length) {
            for (let i = 0; i < objA.length; i++) {
                if (!deepEqual(objA[i], objB[i])) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    } else {
        return a === b ? true : false;
    }
}

const john = {
    firstName: 'John',
    lastName: 'Doe',
    age: 5
}

console.log('Test 1:', deepEqual(1, 1)); // true
console.log('Test 2:', deepEqual(1, '1')); // false
console.log('Test 3:', deepEqual('john', 'john')); // true
console.log('Test 4:', deepEqual(john, { firstName: 'John', lastName: 'Doe' })); // false
console.log('Test 4:', deepEqual(john, { firstName: 'John', lastName: 'Doe', age: 5 })); // true
console.log('Test 5:', deepEqual(john, { firstName: 'John' })); // false
